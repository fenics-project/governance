# Official FEniCS Project Services

The following services are officially operated by the FEniCS Project.

- [Benchbot](http://bamboo.fenicsproject.org/browse/DOL-DB/latest/artifact/JOB1/Benchmarks/index.html)
- [Bitbucket](https://bitbucket.org/fenics-project)
- [Development mailing list](https://groups.google.com/forum/#!forum/fenics-dev)
- [Docker image builder](https://quay.io/organization/fenicsproject)
- [Github](https://github.com/fenics)
- [readthedocs.org](https://fenics.readthedocs.io/en/latest/)
- [Slack](https://fenicsproject.slack.com)
- [Steering Council mailbox](mailto:fenics-steering-council@googlegroups.com)
- [Support mailing list](https://groups.google.com/forum/#!forum/fenics-support)
- [Test system (Bamboo)](https://bamboo.fenicsproject.org/)
- [Twitter feed curated from #fenicsnews tag](https://twitter.com/search?q=%23fenicsnews&src=typd)
- [Ubuntu PPA](https://launchpad.net/~fenics-packages/+archive/ubuntu/fenics)
- [Website](https://fenicsproject.org)
